<?php
require_once("Model/Pgconnection.class.php");
require_once("Model/ValidaEntrata.Class.php");
require_once("Model/Authenticate.class.php");
class Beneficiario{
public $responsavel=null;
 	public static $sqlConfirma ="";
	private static $sqllistaBeneficiarios = "select l.nis,l.nome_pessoa,l.data_nascimento from tb_vl_pessoa as l inner join tb_vl_beneficio as b on l.cod_pessoa=b.codigo_pessoa_beneficio 
 inner join tb_portal_instituicao as i on i.codigo_pan=b.codigo_pan 
 inner join tb_vl_sis_usuario as u on u.codigo_pan=i.codigo_pan 
 where b.flag=2 and u.usuario=:usuario and lower(l.nome_pessoa) like :search and b.flag=2 and b.codigo_programa= :tipoPessoa";
 	public static $sqlBuscaFamilia="select b.cod_pessoa,b.data_nascimento,b.nome_pessoa FROM tb_vl_pessoa as a inner JOIN tb_vl_pessoa AS b ON b.cod_familia=a.cod_familia where a.nis=:nisBeneficiario
 and extract(year from age(b.data_nascimento)) > 16 and b.nis=:nisResp";


	private static $insereRespIdoso="insert into tb_vl_beneficio_idoso (nome,rg) values(:nome , :rg)";
	public $atuthenticate=null;
	public static $_conexao=null;
	public $_valida=null;
	public $user=null;

	function __construct(){
		//Beneficiario::$_conexao=new Pgconnection();
		$this->authenticate = new Authenticate();
		$this->_valida=new ValidaEntrada();
		$this->user=$_SERVER["PHP_AUTH_USER"];

	}


	

	public function confirmaBeneficiario($cod,$nisB,$nisResp,$nomeResp,$rgResp){

        $auth=$this->authenticate->autenticaUser();
       if($auth[0] == false){
            return new soap_fault("Client","","Acesso negado!","Usuário ou senha inválidos");
        }else{}

		
		$codRespCri=0;
		$codResp=0;

	
		
		$vl1=$this->_valida->vldStr($this->user);
		if($vl1[0]==0){
			return new soap_fault('', '', $vl1[1]); 
		}

		if($nisB=='23781489651' and $cod==1 and $nisResp=='23794962121') {
				return array(
					"return"=>1,
					"msgResult"=>"Confirmação realizada com sucesso!",
					"nomeResp"=>'MIGUEL DA SILVA CARVALHO',
					"dtNascimento"=>'1985-05-11'
				);
		}else if($cod==2 and $nisB=='23794987654'){
				return array(
					"return"=>1,
					"msgResult"=>"Confirmação realizada com sucesso!"
				);
		}else{
			return new soap_fault('Client','','Não foi possivel confirmar','Dados incorretos');
		}


}

	public function listaBeneficiarios($cod,$pesq){
			//CONECTA
	
		//
		//
		$auth=$this->authenticate->autenticaUser();
		if($auth[0] == false){
			 return new soap_fault("Client","","Acesso negado!","Usuário ou senha inválidos");
		 }else{}
		$vl1=$this->_valida->vldStr($this->user);
		if($vl1[0]==0){
			return new soap_fault('', '', $vl1[1]); 
		}

		Try{


				$arrPess=array();
			
					$arrPess[]=array("codigoNis"=>'23782883000',"dtNascimento"=>'2017-03-30',"nomePessoa"=>'DAVI LUCAS DASMACENO');
					$arrPess[]=array("codigoNis"=>'23735273420',"dtNascimento"=>'2016-02-08',"nomePessoa"=>'DAVI VIEIRA DE ARRUDA');
					$arrPess[]=array("codigoNis"=>'23781489651',"dtNascimento"=>'2017-05-04',"nomePessoa"=>'PHILIPPE SILVA SOARES');
					$arrPess[]=array("codigoNis"=>'23781285932',"dtNascimento"=>'2016-12-31',"nomePessoa"=>'ARTHUR FELIPE CONSTANTINO DE SALLES');
				
				return array(
					"qtdResult"=>5,
					"pessoas"=>$arrPess
				);


		}Catch(PDOException $e){
			return new soap_fault('', '', 'Ocorreu um erro ao processar os dados !'); 
		}
		
	}

public function insereResponsavel($nomeR,$rgR){
		$cone=Beneficiario::$_conexao->conecta();
		$vl1=$this->_valida->vldNome($nomeR);
		if($vl1[0]==0){
			return  array(false,$vl1[1],"Client"); 
		}

		Try{
			
			$codResp=0;
			$stmtQuery=$cone->prepare(Beneficiario::$insereRespIdoso);
			$stmtQuery->execute(array(
				":nome"=>$nomeR,
				":rg"=>$rgR
			));

			return array(true,$codResp=$cone->lastInsertId());


		}Catch(PDOException $error){
			return array(false,$error->getMessage());
		}
	}
	
}

?>