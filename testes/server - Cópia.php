<?php
require_once("src/lb/NuSOAP/lib/nusoap.php");
require_once("model/Authenticate.class.php");
require_once("model/Entidade.class.php");
require_once("model/Beneficiario.class.php");

        $Authenticate = new Authenticate();
        
		//Instancia do NuSoap
		$server= new soap_server();
        $server->configureWSDL("Viva Leite WS","urn:VivaLeite");
		$server->wsdl->schemaTargetNamespace = "urn:VivaLeite";
		$server->soap_defencoding = 'iso-8859-1';
        $server->decode_utf8 = false;

        $auth=$Authenticate->autenticaUser();
        if($auth[0] == false){
            $server->fault("Client","","Acesso negado!","Usuário ou senha inválidos");
        }else{
            
                    
        }
//#METODO# BUSCA DADOS DA ENTIDADE
        $server->register('Entidade.dadosEntidade',
         array(),
         array('return' => 'xsd:int',
         'msgResult' => 'xsd:string',
         'rsEntidade' => 'xsd:string',
         'enderecoEntidade' => 'xsd:string',
         'telEntidade' => 'xsd:string',
         'emailEntidade' => 'xsd:string',
         'telPresidente' => 'xsd:string',
         'emailPresidente' => 'xsd:string'
        ),
         'urn:VivaLeite.Entidade',
         'urn:VivaLeite.Entidade',
         'rpc',
         'encoded',
         'Método que retorna os dados da entidade que são exibidos na tela inicial do APP.'
        );


//#METODO# LISTA BENEFICIARIOS
$server->wsdl->addComplexType('structListaPessoas','Element','struct','all','',
        array(
            'nomePessoa' => array('name' => 'nomePessoa', 'type' => 'xsd:string'),
            'dtNascimento' => array('name' => 'dtNascimento', 'type' => 'xsd:string'),
            'codigoNis' => array('name' => 'codigoNis', 'type' => 'xsd:string')
        )
);
// Complex Array ++++++++++++++++++++++++++++++++++++++++++
$server->wsdl->addComplexType('listaPessoas','complexType','array','','SOAP-ENC:Array',
        array(/*'transmiteRefEstrutura' =>array ('name' => 'transmiteRefEstrutura', 'type' => 'tns:transmiteRefEstrutura')*/),
        array(
            array(
                'ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:structListaPessoas[]'
            )
        ),"tns:structListaPessoas"
);
        $server->register('Beneficiario.listaBeneficiarios',
         array("codigoPessoa"=>"xsd:int","pesquisaPessoa"=>"xsd:string"),
         array('qtdResult' => 'xsd:int',
         'pessoas' => 'tns:listaPessoas'
        ),
         'urn:VivaLeite.Beneficiarios',
         'urn:VivaLeite.Beneficiarios',
         'rpc',
         'encoded',
         'Retorna a lista de Beneficiarios pendentes de confirmação.'
        );
//#METODO# BCONFIRMA BENEFICIARIO
        $server->register('Beneficiario.confirmaBeneficiario',
         array("codigoPessoa"=>"xsd:int",
            "nisBeneficiario"=>"xsd:int",
            "nisResponsavel"=>"xsd:int",
            "nomeResponsavel"=>"xsd:string",
            "rgResponsavel"=>"xsd:string"),
         array('return' => 'xsd:int',
         'msgResult' => 'xsd:string'
        ),
         'urn:VivaLeite.Beneficiario',
         'urn:VivaLeite.Beneficiario',
         'rpc',
         'encoded',
         'Realiza a confirmação do beneficiário.'
        );
            $server->service(file_get_contents("php://input"));

        

?>