<?php
require_once("vendor/autoload.php");
require_once("src/lb/NuSOAP/lib/nusoap.php");
require_once("Model/Entidade.class.php");
require_once("Model/Beneficiario.class.php");

		//Instancia do NuSoap
		$server= new soap_server();
                $server->configureWSDL("Viva Leite WS","urn:VivaLeite");
		$server->wsdl->schemaTargetNamespace = "urn:VivaLeite";
		$server->soap_defencoding = 'iso-8859-1';
                $server->decode_utf8 = false;



//#METODO# BUSCA DADOS DA ENTIDADE
        $server->register('Entidade.dadosEntidade',
         array(),
         array('return' => 'xsd:int',
         'msgResult' => 'xsd:string',
         'rsEntidade' => 'xsd:string',
         'enderecoEntidade' => 'xsd:string',
         'telEntidade' => 'xsd:string',
         'emailEntidade' => 'xsd:string',
         'telPresidente' => 'xsd:string',
         'emailPresidente' => 'xsd:string'
        ),
         'urn:VivaLeite.Entidade',
         'urn:VivaLeite.Entidade',
         'rpc',
         'encoded',
         'Método que retorna os dados da entidade que são exibidos na tela inicial do APP.'
        );


//#METODO# LISTA BENEFICIARIOS
$server->wsdl->addComplexType('structListaPessoas','Element','struct','all','',
        array(
            'nomePessoa' => array('name' => 'nomePessoa', 'type' => 'xsd:string'),
            'dtNascimento' => array('name' => 'dtNascimento', 'type' => 'xsd:string'),
            'codigoNis' => array('name' => 'codigoNis', 'type' => 'xsd:string')
        )
);
// Complex Array ++++++++++++++++++++++++++++++++++++++++++
$server->wsdl->addComplexType('listaPessoas','complexType','array','','SOAP-ENC:Array',
        array(/*'transmiteRefEstrutura' =>array ('name' => 'transmiteRefEstrutura', 'type' => 'tns:transmiteRefEstrutura')*/),
        array(
            array(
                'ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:structListaPessoas[]'
            )
        ),"tns:structListaPessoas"
);
        $server->register('Beneficiario.listaBeneficiarios',
         array("codigoPessoa"=>"xsd:int","pesquisaPessoa"=>"xsd:string"),
         array('qtdResult' => 'xsd:int',
         'pessoas' => 'tns:listaPessoas'
        ),
         'urn:VivaLeite.Beneficiarios',
         'urn:VivaLeite.Beneficiarios',
         'rpc',
         'encoded',
         'Retorna a lista de Beneficiarios pendentes de confirmação.'
        );
//#METODO# BCONFIRMA BENEFICIARIO
        $server->register('Beneficiario.confirmaBeneficiario',
         array("codigoPessoa"=>"xsd:int",
            "nisBeneficiario"=>"xsd:string",
            "nisResponsavel"=>"xsd:string",
            "nomeResponsavel"=>"xsd:string",
            "rgResponsavel"=>"xsd:string"),
         array('return' => 'xsd:int',
         'msgResult' => 'xsd:string',
         'nomeResp' => 'xsd:string',
         'dtNascimento' => 'xsd:date'
        ),
         'urn:VivaLeite.Beneficiario',
         'urn:VivaLeite.Beneficiario',
         'rpc',
         'encoded',
         'Realiza a confirmação do beneficiário.'
        );
		
		
//#METODO# BUSCA DADOS DA ENTIDADE
        $server->register('Authenticate.autenticaUsuario',
         array(),
         array('return' => 'xsd:int'
        ),
         'urn:VivaLeite.Authenticate',
         'urn:VivaLeite.Authenticate',
         'rpc',
         'encoded',
         'Autentica usuario.'
        );
		
            $server->service(file_get_contents("php://input"));

        

?>