<?php
require_once ("Pgconnection.class.php");
class Authenticate{
	private $cone=null;
	private static $sqlLoginSelect="SELECT usuario, nome, codigo_perfil, codigo_municipio, codigo_drads, 
	senha, email, flag, data_cadastro, id, codigo_departamento, codigo_pan
FROM tb_vl_sis_usuario where usuario= :usuario and flag=0";
	public $pan = null;

	public function __construct(){
		$conn=new Pgconnection();
		//$this->cone=$this->cone->conecta();
		$this->cone=$conn->conecta();

	}

	public function autenticaUsuario(){
		if (isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW'])) {
			$user=$_SERVER['PHP_AUTH_USER'];
			$pass=$_SERVER['PHP_AUTH_PW'];

			$checa=$this->checaDb($user,$pass);
			if($checa[0]==false){

				return 0;
			}else{
				return 1;
			}

		}else{
			
			return 0;
		}
	}
	
	public function autenticaUser(){
		
		if (isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW'])) {
			$user=$_SERVER['PHP_AUTH_USER'];
			$pass=$_SERVER['PHP_AUTH_PW'];

			$checa=$this->checaDb($user,$pass);
			if($checa[0]==false){

				return array(false,$checa[1],$checa[2]);
			}else{
				return array(true,"","");
			}

		}else{
			
			return array(false,"Usuário ou senha não informados","Client");
		}
	}

	public function checaDb($user,$pass){
		

		if($this->cone==false){

			return array(false,"Conexão perdida","Server");

		}else{
			
			$buscaUsr=$this->cone->prepare(Authenticate::$sqlLoginSelect);
			$bind=array(":usuario"=>$user);
			$buscaUsr->execute($bind);
			$countLine=$buscaUsr->rowCount();
			
			$fetch=$buscaUsr->fetch(PDO::FETCH_ASSOC);

			if(crypt($pass,$fetch["senha"])==$fetch["senha"]){
				$this->setPan($fetch["codigo_pan"]);

				return array(true,"","");
			}else{
				return array(false,"Usuário ou senha inválidos","Client");
			}

			
		}


	}

	public function setPan($pan){
		$this->pan=$pan;
	}

	public function getPan(){
		return $this->pan;
	}

}

?>