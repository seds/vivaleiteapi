<?php
require_once("Model/Pgconnection.class.php");
require_once("Model/ValidaEntrata.Class.php");
require_once("Model/Authenticate.class.php");

class Entidade{
	private static $dadosEntidade = "select a.razao_social,a.logradouro,a.telefone,a.email,b.telefone as ptelefone,b.email as pemail from tb_portal_instituicao as a inner join
	tb_vl_rmsp_instituicao_responsavel as b on a.codigo_pan=b.codigo_pan inner join tb_vl_sis_usuario as c on b.codigo_pan=c.codigo_pan where flag=0 and c.usuario=:usuario";
	public static $_conexao=null;
	public $_valida=null;
	public $user=null;
	public $authenticate = null;

	function __construct(){

		//CONEXAO
		Entidade::$_conexao=new Pgconnection();
		//AUTENTICA
		$this->authenticate=new Authenticate(Entidade::$_conexao);
		Entidade::$_conexao=Entidade::$_conexao->conecta();
		//VALIDACOES
		$this->_valida=new ValidaEntrada();
		$this->user=$_SERVER["PHP_AUTH_USER"];
		
	}


	public function dadosEntidade(){
		//CONECTA
		if(Entidade::$_conexao==false){
			return new soap_fault('Server', '', "Erro de Coonexão"); 
		}

		//AUTENTICA USUARIO
        $auth=$this->authenticate->autenticaUser();
        if($auth[0] == false){
            return new soap_fault($auth[2],"",$auth[1]);
        }else{}
	
		
		$vl1=$this->_valida->vldStr($this->user);
		if($vl1[0]==0){
			return new soap_fault('', '', $vl1[1]); 
		}

		Try{
			$stmtQuery=Entidade::$_conexao->Prepare(Entidade::$dadosEntidade);
			$stmtQuery->bindValue(":usuario",$this->user,PDO::PARAM_INT);
			$stmtQuery->execute();
			
			$countQuery=$stmtQuery->rowCount();
	

			//BUSCA DADOS DA ENTIDADE
			If($countQuery>0){

				$entidade=$stmtQuery->fetch(PDO::FETCH_ASSOC);
				return array(
					"resultado"=>1,
					"msgResult"=>"Ok",
					"rsEntidade"=>$entidade["razao_social"],
					"enderecoEntidade"=>$entidade["logradouro"],
					"telEntidade"=>$entidade["telefone"],
					"emailEntidade"=>$entidade["email"],
					"telPresidente"=>$entidade["ptelefone"],
					"emailPresidente"=>$entidade["pemail"],
				);
			}else{//SE NAO HOUVER RESULTADOS PARA A QUERY PAI
				return new soap_fault('Client', '', 'Não houveram resultados!','Por favor refaça o login'.Entidade::$dadosEntidade); 
			}

		}Catch(PDOException $e){
			return new soap_fault('', '', 'Ocorreu um erro ao processar os dados !'); 
		}
		
	}


	
}

?>