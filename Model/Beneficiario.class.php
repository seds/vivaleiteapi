<?php
require_once("Model/Pgconnection.class.php");
require_once("Model/ValidaEntrata.Class.php");
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Beneficiario{
public $responsavel=null;
	 public static $sqlConfirma ="";
	 private $log=null;
	private static $sqllistaBeneficiarios = "select l.nis,l.nome_pessoa,l.data_nascimento from tb_vl_pessoa as l inner join tb_vl_beneficio as b on l.cod_pessoa=b.codigo_pessoa_beneficio 
 inner join tb_portal_instituicao as i on i.codigo_pan=b.codigo_pan 
 inner join tb_vl_sis_usuario as u on u.codigo_pan=i.codigo_pan 
 where b.flag=2 and u.usuario=:usuario and lower(l.nome_pessoa) like :search and b.codigo_programa= :tipoPessoa";
 	public static $sqlBuscaFamilia=" select b.cod_pessoa,b.nis,b.data_nascimento,b.nome_pessoa FROM tb_vl_pessoa as b where 
	 extract(year from age(b.data_nascimento)) > 16 and b.nis=:nisResp and b.cod_familia=(select cod_familia from tb_vl_pessoa where nis=:nisBeneficiario);";
  	public static $sqlBuscaPessoa="select b.cod_pessoa,b.nome_pessoa,b.data_nascimento FROM tb_vl_pessoa as b where b.nis=:nisPessoa";
///git update

	private static $insereRespIdoso="insert into tb_vl_beneficio_idoso (nome,rg) values(:nome , :rg) returning codigo_responsavel_idoso";

	public static $_conexao=null;
	public $_valida=null;
	public $user=null;
	public $authenticate = null;
	function __construct(){
		// create a log channel
		$this->log = new Logger('name');
		$this->log->pushHandler(new StreamHandler('log.log', Logger::WARNING));

		//CONEXAO
		Beneficiario::$_conexao=new Pgconnection();
		//AUTENTICA
		$this->authenticate=new Authenticate(Beneficiario::$_conexao);

		Beneficiario::$_conexao=Beneficiario::$_conexao->conecta();
		//VALIDACOES
		$this->_valida=new ValidaEntrada();
		$this->user=$_SERVER["PHP_AUTH_USER"];

	}
	private function setLog($msg){
			$this->log->warning($msg." | Class= Beneficiario");
	}
	public function checaFamilia($nisCri,$nisResp){
		//new$cone=Beneficiario::$_conexao->conecta();

		try{
			$stmtQuery=Beneficiario::$_conexao->prepare(Beneficiario::$sqlBuscaFamilia);
			$stmtQuery->execute(array("nisBeneficiario"=>$nisCri,"nisResp"=>$nisResp));
			$count=$stmtQuery->rowCount();
			$fetch=$stmtQuery->fetch(PDO::FETCH_ASSOC);

			if($count==1){
				return array(true,$fetch);
			}else{
				return array(false,"Responsável não localizado ou não se encaixa nas especificações.");
			}


		}Catch(PDOException $error){
			//URGENTecho $error->getMessage();
		}
	}

public function buscaPessoa($nis){
		//new$cone=Beneficiario::$_conexao->conecta();

		try{
			$stmtQuery=Beneficiario::$_conexao->prepare(Beneficiario::$sqlBuscaPessoa);
			$stmtQuery->execute(array(":nisPessoa"=>$nis));
			$count=$stmtQuery->rowCount();
			$fetch=$stmtQuery->fetch(PDO::FETCH_ASSOC);

			if($count==1){
				return array(true,$fetch);
			}else{
				return array(false,"Responsável não localizado ou não se encaixa nas especificações.");
			}


		}Catch(PDOException $error){
			//URGENTecho $error->getMessage();
		}
	}

	public function confirmaBeneficiario($cod,$nisB,$nisResp,$nomeResp1,$rgResp){
		//CONECTA
		//new$cone2=Beneficiario::$_conexao->conecta();
		//
		//
		//AUTENTICA USUARIO
        $auth=$this->authenticate->autenticaUser();
        if($auth[0] == false){
            return new soap_fault($auth[2],"",$auth[1]);
		}else{}

		//Param
		$nomeresp="";
		$dataNasc="";
	
		Beneficiario::$_conexao->beginTransaction();
		$codRespCri=0;
		$codResp=0;

		//SE FOR IDOSO
		if($cod==2){

				//VERIFICA SE IDOSO É PROPRIO RESPONSAVEL
				if(((isset($rgResp) && strlen($rgResp)>0) || (isset($nomeResp1) && strlen($nomeResp1)>0)) && (isset($nisResp) && strlen($nisResp)>0)){
					return new soap_fault('Client', '', 'Ocorreu um erro ao confirmar',"Se idoso não for o responsavel, não preencher o atributo nisResponsavel."); 
				}else if((isset($rgResp) && strlen($rgResp)>0) || (isset($nomeResp1) && strlen($nomeresp1)>0)){
						//INSERE RESPONSAVEL EXTERNO

						$vl2=$this->_valida->vldNome($nomeResp1);
						$vl4=$this->_valida->vldRg($rgResp);
						if($vl2[0]==0 || $vl4[0]==0){
							return new soap_fault('', '', $vl2[1].".".$vl4[1]); 
						}

						$codResp=$this->insereResponsavel($nomeResp1,$rgResp);
						if($codResp[0]){
								$this->setLog("[".$_SERVER['PHP_AUTH_USER']."] Inseriu responsavel: ".$codResp[1]);
								$responsavel="codigo_responsavel_idoso=".$codResp[1]." ,";
								$nomeresp=$nomeResp1;
						}else{
							Beneficiario::$_conexao->rollBack();
							$this->setLog("[".$_SERVER['PHP_AUTH_USER']."] Responsável [idoso] não inserido: ".$nomeresp1);
							return new soap_fault($codResp[2], '', $codResp[1],"Ocorreu um erro ao processar a solicitação"); 
							die;
						}

					}else if((isset($nisResp) && strlen($nisResp)>0) && ($nisB == $nisResp)){
						
						$codResp=$this->buscaPessoa($nisB);
					
						if($codResp[0]===true){
							$this->setLog("[".$_SERVER['PHP_AUTH_USER']."] Idoso é o proprio responsavel... ".$nisB);
							$responsavel="codigo_pessoa_responsavel=null,";
							$nomeresp=$codResp[1]['nome_pessoa'];
						}else{
							$this->setLog("[".$_SERVER['PHP_AUTH_USER']."] Não foi possivel confirmar o beneficio [crianca]: ".$nisB);
							return new soap_fault('', '', 'Ocorreu um erro ao confirmar',"Ocorreu um erro ao processar a solicitação"); 
						}

					}else{
							$this->setLog("[".$_SERVER['PHP_AUTH_USER']."] Não foi possivel confirmar o beneficio [crianca]. Verifique os parametros enviados: ".$nisB);
							return new soap_fault('Client', '', 'Ocorreu um erro ao confirmar',"Alguns parametros não foram transmitidos."); 
					}
		//SE FOR CRIANÇA
		}else if($cod==1){
			$codRespCri=$this->checaFamilia($nisB,$nisResp);
		
			if($codRespCri[0]){
				$this->setLog("[".$_SERVER['PHP_AUTH_USER']."] Familia da crianca/responsavel validada... ".$nisB);
				$responsavel="codigo_pessoa_responsavel=".$codRespCri[1]["cod_pessoa"]." ,";
				$nomeresp=$codRespCri[1]["nome_pessoa"];
				$dataNasc=$this->_valida->converteDataUsToBr($codRespCri[1]["data_nascimento"]);
			}else{
				Beneficiario::$_conexao->rollBack();
				$this->setLog("[".$_SERVER['PHP_AUTH_USER']."] Erro ao confirmar crianca. Verifique se o responsavel é da mesma familia. ".$nisB);
				return new soap_fault('', '', 'Ocorreu um erro ao confirmar',"Ocorreu um erro ao processar a solicitação"); 
			}
			
		}else{
			Beneficiario::$_conexao->rollBack();
			$this->setLog("[".$_SERVER['PHP_AUTH_USER']."] O codigo do beneficiario nao foi informado corretamente. ".$nisB);
			return new soap_fault('', '', 'Ocorreu um erro ao confirmar',"O código da pessoa deve ser 1 ou 2"); 
		}
	
		$vl1=$this->_valida->vldStr($this->user);
		if($vl1[0]==0){
			Beneficiario::$_conexao->rollBack();
			$this->setLog("[".$_SERVER['PHP_AUTH_USER']."] Usuario incorreto. ".$nisB);
			return new soap_fault('', '', $vl1[1]); 
		}

		try{
			$updtCod="update tb_vl_beneficio as b SET $responsavel flag = 0, data_confirmacao = NOW(), data_inicio_beneficio =NOW(), codigo_desvinculo = NULL from tb_vl_pessoa as p, tb_vl_sis_usuario as u ,tb_portal_instituicao as i WHERE b.codigo_pessoa_beneficio = p.cod_pessoa and p.nis=:nisPessoa and i.codigo_pan=b.codigo_pan and u.codigo_pan=i.codigo_pan and u.usuario=:usuario and b.flag=2 and b.codigo_programa=".$cod;
			
			$stmtQuery=Beneficiario::$_conexao->prepare($updtCod);
			$stmtQuery->execute(array(
				":usuario",$this->user,
				":nisPessoa"=>$nisB
			));
			$countQuery=$stmtQuery->rowCount();

			//BUSCA DADOS DA Beneficiario
			If($countQuery>0){
				Beneficiario::$_conexao->commit();
				$this->setLog("[".$_SERVER['PHP_AUTH_USER']."] Beneficio confirmado com sucesso! ".$nisB);
				return array(
					"return"=>$countQuery,
					"msgResult"=>"Confirmação realizada com sucesso!",
					"nomeResp"=>$nomeresp,
					"dtNascimento"=>$dataNasc
				);
			}else{//SE NAO HOUVER RESULTADOS PARA A QUERY PAI
				//echo var_dump($stmtQuery);
				$this->setLog("[".$_SERVER['PHP_AUTH_USER']."] Nao foi possivel confirmar o beneficio(0)  ".$nisB);
				Beneficiario::$_conexao->rollBack();
				return new soap_fault('', '', 'Falha ao confirmar beneficiario',"Verifique se o mesmo já está confirmado ou o tipo de pessoa está correto.".$updtCod); 

			}
	}Catch(PDOException $error){
		$this->setLog("[".$_SERVER['PHP_AUTH_USER']."] Nao foi possivel confirmar o beneficio ".$nisB." ->".$error->getMessage());
		Beneficiario::$_conexao->rollBack();
		return new soap_fault('', '', 'Ocorreu um erro ao processar os dados !'); 
	}
}

	public function listaBeneficiarios($cod,$pesq){
		//CONECTA
		//newBeneficiario::$_conexao=Beneficiario::$_conexao->conecta();
		//
		//
		//AUTENTICA USUARIO
        $auth=$this->authenticate->autenticaUser();
        if($auth[0] == false){
            return new soap_fault($auth[2],"",$auth[1]);
		}else{}
		//VALIDA ENTRADAS
		$vl1=$this->_valida->vldStr($this->user);
		if($vl1[0]==0){
			return new soap_fault('', '', $vl1[1]); 
		}
		//END

		Try{
			$stmtQuery=Beneficiario::$_conexao->Prepare(Beneficiario::$sqllistaBeneficiarios);
			$stmtQuery->bindValue(":usuario",$this->user,PDO::PARAM_STR);
			$stmtQuery->bindValue(":search",$pesq.'%',PDO::PARAM_STR);
			$stmtQuery->bindValue(":tipoPessoa",$cod,PDO::PARAM_INT);
			$stmtQuery->execute();
			
			$countQuery=$stmtQuery->rowCount();

			//BUSCA DADOS DA Beneficiario
			If($countQuery>0){
				$arrPess=array();
				while($Beneficiario=$stmtQuery->fetch(PDO::FETCH_ASSOC)){
					$arrPess[]=array("codigoNis"=>$Beneficiario["nis"],"dtNascimento"=>$this->_valida->converteDataUsToBr($Beneficiario["data_nascimento"]),"nomePessoa"=>$Beneficiario["nome_pessoa"]);
				}
					return array(
						"qtdResult"=>$countQuery,
						"pessoas"=>$arrPess
					);
			}else{//SE NAO HOUVER RESULTADOS PARA A QUERY PAI

				return array(

					"qtdResult"=>0
				);
			}

		}Catch(PDOException $e){
			return new soap_fault('', '', 'Ocorreu um erro ao processar os dados !'); 
		}
		
	}

public function insereResponsavel($nomeR,$rgR){
		//new$cone=Beneficiario::$_conexao->conecta();
		$this->setLog("##### Inserindo responsável...");
		$vl1=$this->_valida->vldNome($nomeR);
		if($vl1[0]==0){
			$this->setLog("# Validação de nome inválida");
			Beneficiario::$_conexao->rollBack();
			return  array(false,$vl1[1],"Client"); 
		}

		try{
			
			$codResp=0;
			$stmtQuery=Beneficiario::$_conexao->prepare(Beneficiario::$insereRespIdoso);
			$stmtQuery->execute(array(
				":nome"=>$nomeR,
				":rg"=>$rgR
			));
			
			if($stmtQuery->rowCount()>0){
				$this->setLog("# Responsavel inserido");
				return array(true,$codResp=$stmtQuery->fetch(PDO::FETCH_ASSOC)["codigo_responsavel_idoso"]);
			}else{
				$this->setLog("# Não foi possivel recuperar o codigo do responsavel");
				return  array(false,"Não foi possível inserir o responsável.","Client"); 
			}
			


		}Catch(PDOException $error){
			$this->setLog("# Erro ao inserir responsável: ".$error->getMessage());
			Beneficiario::$_conexao->rollBack();
			return array(false,$error->getMessage());
		}
	}
	
}

?>