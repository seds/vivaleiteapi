﻿1.3.   

1.3.3.      Query de retorno
SELECT codigo_carta_recibo, num_carta, serie_carta, ano_carta, num_processo, ano_processo FROM bp_carta_recibo WHERE flag = 0 AND req_sync = 'S' AND codigo_unidade = ?


 

1.7.   ConsultaCartaReciboCancelada

1.7.3.      Query de retorno
SELECT codigo_carta_recibo, num_carta, serie_carta, ano_carta, data_cancelamento FROM bp_carta_recibo WHERE flag = 1 AND data_cancelamento >= NOW() – INTERVAL ’30 days’ AND codigo_unidade = ?
 
1.8.   ConsultaDadosRestaurante

1.8.3.      Query de retorno
SELECT nome_unidade, razao_social, cnpj, endereco, telefone, cidade, responsavel, dados_banc_banco, dados_banc_agencia, dados_banc_cc, dados_pref_banco, dados_pref_agencia, dados_pref_cc, nome_orgao, nome_prefeitura FROM vi_dados_unidades WHERE codigo_unidade = ?
